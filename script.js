document.addEventListener('scroll' , function() {
    const navbar = document.querySelector('.navbar')
    if(window.scrollY > 100) {
        navbar.classList.add('navOnScroll')
    } else {
        navbar.classList.remove('navOnScroll')
    }
})